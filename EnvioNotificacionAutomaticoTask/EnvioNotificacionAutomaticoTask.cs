﻿using BOWcfMailsAutomaticosProductos;
using DllWcfUtility;
using ServiceMailsProductos;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceModel.Syndication;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace EnvioNotificacionAutomaticoTask
{
    class EnvioNotificacionAutomaticoTask
    {
        private static ILog log = LogManager.GetLogger("FileLog");
        private static ILog logError = LogManager.GetLogger("FileLogError");

        static void Main(string[] args)
        {
            log.Info("EnvioNotificacionAutomaticoTask.Main Start");
            try
            {
                ServicioMailAutomaticoProductoClient client;
                
                BeanList<AutomaticNotification> notifications;
                client = new ServicioMailAutomaticoProductoClient();

                notifications = client.GetAutomaticNotifications();
                if (notifications != null && notifications.beanList != null)
                {
                    foreach (AutomaticNotification notification in notifications.beanList)
                    {
                        log.Info("EnvioNotificacionAutomaticoTask.Main Enviar notificación a " + notification.Email);
                        MailParameter[] parameters;
                        EMailRecipient[] recipients;
                        EMailRecipient recipient;
                        parameters = new MailParameter[2];
                        recipients = new EMailRecipient[1];
                        parameters[0] = new MailParameter(DllConstants.Constants.enuAutomaticMailParameterKey.RECIPIENT_NAME, notification.UserName);
                        parameters[1] = new MailParameter(DllConstants.Constants.enuAutomaticMailParameterKey.PRODUCT_NAME, notification.NavisionProductDescription);
                        recipient = new EMailRecipient();
                        recipient.Name = notification.UserName;
                        recipient.Email = notification.Email;
                        recipients[0] = recipient;
                        int result = client.PeticionEnvioAutomaticoMailsParametrosGrupo(parameters, recipients, "CLAVES", notification.Template, "Remitente", notification.Subject, 1);
                        log.Info("EnvioNotificacionAutomaticoTask.Main Petición de envío realizada " + result);
                        if (result > 0)
                        {
                            client.UpdateAutomaticNotification(notification.AutomaticNotificationId, result);
                        }
                    }
                }
            }
            catch (Exception err)
            {
                logError.Error("EnvioNotificacionAutomaticoTask.Main Error: " + err.Message);
            }

            log.Info("EnvioNotificacionAutomaticoTask.Main End");
        }

    }
}
